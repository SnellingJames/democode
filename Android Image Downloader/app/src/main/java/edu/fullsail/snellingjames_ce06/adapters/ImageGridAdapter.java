//James Snelling
//JAV2 - 201811
//ImageGridAdapter

package edu.fullsail.snellingjames_ce06.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import edu.fullsail.snellingjames_ce06.R;

public class ImageGridAdapter extends BaseAdapter {

    //context
    private final Context mContext;

    //id
    private static final int ID_CONSTANT = 0x01000000;

    //arrayList to hold image Uris
    private final ArrayList<Uri> mArrayList;

    //constructor
    public ImageGridAdapter(Context context, ArrayList<Uri> list){
        mContext = context;
        mArrayList = list;
    }

    @Override
    public int getCount() {
        if (mArrayList != null){
            return mArrayList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (mArrayList != null && position<mArrayList.size() && position>=0){
            return mArrayList.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return ID_CONSTANT + position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //variable for ViewHolder
        GridViewHolder viewHolder;

        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.grid_item, parent, false);

            //creating a new ViewHolder to hold the convertView fields
            viewHolder = new GridViewHolder();
            viewHolder.appImageView = convertView.findViewById(R.id.imageDisplay);

            //setting the convertView's tag as the newly created ViewHolder
            convertView.setTag(viewHolder);

        } else {

            viewHolder = (GridViewHolder) convertView.getTag();

        }

        Object item = getItem(position);

        if (item != null){

            //create new Bitmap
            Bitmap bmp;
            try{
                //getting bitmap from URI
                InputStream input = mContext.getContentResolver().openInputStream(mArrayList.get(position));
                bmp = BitmapFactory.decodeStream(input);

                //setting scaled down bitmap to imageView
                viewHolder.appImageView.setImageBitmap(Bitmap.createScaledBitmap(bmp, 175, 175, false));

            } catch (IOException e){
                Log.i("Bitmap Error", "File not found", e);
            }
        }

        return convertView;
    }

    //static class for the ViewHolder
    static class GridViewHolder{
        ImageView appImageView;
    }
}
