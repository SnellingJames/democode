//James Snelling
//JAV2 - 201811
//MainActivity.java

package edu.fullsail.snellingjames_ce06;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import edu.fullsail.snellingjames_ce06.fragments.GridFragment;

public class MainActivity extends AppCompatActivity implements GridFragment.GridListener {

    //action for broadcast receiver
    private static final String ACTION_NEW_IMAGE = "edu.fullsail.snellingjames_ce06.ACTION_NEW_IMAGE";

    //authority
    private static final String AUTHORITY = "edu.fullsail.snellingjames_ce06.fileprovider";

    //variable for broadcast receiver
    private ImageReceiver mReceiver;

    //arrayList to hold Uris
    private ArrayList<Uri> imageUris;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //instantiate arrayList
        imageUris = new ArrayList<>();

        //loading fragment
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragmentContainer, GridFragment.newInstance(), GridFragment.TAG)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //registering receiver
        mReceiver = new ImageReceiver();

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_NEW_IMAGE);

        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        //unregister receiver
        unregisterReceiver(mReceiver);
    }

    //method to get Uris to display images in grid
    private void getUris(){

        imageUris.clear();

        File protectedStorage = getExternalFilesDir("images");
        File[] fileNames = Objects.requireNonNull(protectedStorage).listFiles();

        for (File fileName : fileNames) {
            Uri fileUri = FileProvider.getUriForFile(this, AUTHORITY, fileName);
            imageUris.add(fileUri);
        }
    }

    //interface method to send images to fragment
    @Override
    public ArrayList<Uri> loadImages() {
        return imageUris;
    }

    //interface method to send selected image to viewer
    @Override
    public void sendSelectedImage(Uri imageUri) {

        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setDataAndType(imageUri, "image/jpeg");
        sendIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        this.startActivity(sendIntent);

    }

    //broadcast receiver
    private class ImageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(Objects.requireNonNull(intent.getAction()).equals(ACTION_NEW_IMAGE)){

                getUris();

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragmentContainer, GridFragment.newInstance(), GridFragment.TAG)
                        .commit();
            }
        }
    }
}
