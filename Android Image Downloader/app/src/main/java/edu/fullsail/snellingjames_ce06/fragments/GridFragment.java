//James Snelling
//JAV2 - 201811
//GridFragment.java

package edu.fullsail.snellingjames_ce06.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import edu.fullsail.snellingjames_ce06.R;
import edu.fullsail.snellingjames_ce06.adapters.ImageGridAdapter;
import edu.fullsail.snellingjames_ce06.services.ImageIntentService;

public class GridFragment extends Fragment {

    //tag
    public static final String TAG = "GridFragment.TAG";

    //interface
    private GridListener mListener;

    //arrayList for the image Uris
    private ArrayList<Uri> imageUris;

    //interface
    public interface GridListener{
        ArrayList<Uri> loadImages();
        void sendSelectedImage(Uri imageUri);
    }

    //method to access the interface
    private ArrayList<Uri> getImages(){
        return mListener.loadImages();
    }

    //method to access the interface
    private void sendImageUri(Uri imageUri){
        mListener.sendSelectedImage(imageUri);
    }

    //new instance
    public static GridFragment newInstance() {

        return new GridFragment();
    }

    //checking implementation
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof GridListener){
            mListener = (GridListener) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //instantiating arrayList
        imageUris = new ArrayList<>();

        //setting that menu exists
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.download_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //starting the intentService
        Intent downloadIntent = new Intent(getContext(), ImageIntentService.class);
        Objects.requireNonNull(getActivity()).startService(downloadIntent);
        return super.onOptionsItemSelected(item);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image_grid, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //hooking up the grid
        GridView imageGrid = Objects.requireNonNull(getView()).findViewById(R.id.imageGrid);

        //variable for empty grid textView
        TextView emptyText = getView().findViewById(R.id.emptyText);
        emptyText.setText(R.string.please_download);
        imageGrid.setEmptyView(emptyText);

        //grid click listener
        imageGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Uri selectedUri = imageUris.get(i);
                sendImageUri(selectedUri);
            }
        });

        //getting the URIs
        imageUris = getImages();

        //creating adapter
        ImageGridAdapter gridAdapter = new ImageGridAdapter(getContext(), imageUris);

        //setting adapter if list is greater than 0
        if(imageUris.size() != 0){
            imageGrid.setAdapter(gridAdapter);
        }
    }

}
