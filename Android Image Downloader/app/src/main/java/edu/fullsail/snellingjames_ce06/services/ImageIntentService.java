//James Snelling
//JAV2 - 201811
//ImageIntentService.java

package edu.fullsail.snellingjames_ce06.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class ImageIntentService extends IntentService {

    //action variable
    private static final String ACTION_NEW_IMAGE = "edu.fullsail.snellingjames_ce06.ACTION_NEW_IMAGE";

    //file names
    private final String[] IMAGES = {
            "Df9sV7x.jpg", "nqnegVs.jpg", "JDCG1tP.jpg",
            "tUvlwvB.jpg", "2bTEbC5.jpg", "Jnqn9NJ.jpg",
            "xd2M3FF.jpg", "atWe0me.jpg", "UJROzhm.jpg",
            "4lEPonM.jpg", "vxvaFmR.jpg", "NDPbJfV.jpg",
            "ZPdoCbQ.jpg", "SX6hzar.jpg", "YDNldPb.jpg",
            "iy1FvVh.jpg", "OFKPzpB.jpg", "P0RMPwI.jpg",
            "lKrCKtM.jpg", "eXvZwlw.jpg", "zFQ6TwY.jpg",
            "mTY6vrd.jpg", "QocIraL.jpg", "VYZGZnk.jpg",
            "RVzjXTW.jpg", "1CUQgRO.jpg", "GSZbb2d.jpg",
            "IvMKTro.jpg", "oGzBLC0.jpg", "55VipC6.jpg"
    };


    //default constructor
    public ImageIntentService() {
        super("ImageIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        //iterating through image array
        for(String image:IMAGES){

            //getting storage path
            File protectedStorage = getExternalFilesDir("images");

            //creating new file
            File imageFile = new File(protectedStorage, image);

            //if file exists send broadcast to update UI
            if(imageFile.exists()){

                System.out.println("File Already Exists");
                Intent doneIntent = new Intent(ACTION_NEW_IMAGE);
                sendBroadcast(doneIntent);

            } else {

                //if file doesn't exist
                try{
                    //creating full URL
                    String URL_BASE = "https://i.imgur.com/";
                    URL url = new URL(URL_BASE + image);
                    InputStream input = url.openStream();

                    //create outputStream file while reading from inputStream
                    OutputStream output = new FileOutputStream(new File(protectedStorage, image));
                    byte[] buffer = new byte[2048];
                    int bytesRead;
                    while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                        output.write(buffer, 0, bytesRead);
                    }

                    output.close();
                    input.close();

                    //update UI
                    Intent doneIntent = new Intent(ACTION_NEW_IMAGE);
                    sendBroadcast(doneIntent);

                    System.out.println("File Written");

                }catch (MalformedURLException e){
                    Log.i("Intent Service", "There was a problem with the URL", e);
                }catch (IOException e){
                    Log.i("Intent Service", "There was an error getting the image", e);
                }
            }
        }
    }
}
