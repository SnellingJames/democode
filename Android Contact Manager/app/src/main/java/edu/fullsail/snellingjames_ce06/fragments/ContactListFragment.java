package edu.fullsail.snellingjames_ce06.fragments;

//James Snelling
//AID - 201809
//ContactListFragment.java

import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import java.util.ArrayList;
import edu.fullsail.snellingjames_ce06.Contact;
import edu.fullsail.snellingjames_ce06.ListAdapter;
import edu.fullsail.snellingjames_ce06.R;

public class ContactListFragment extends ListFragment {

    //interface context variable
    private ContactsListListener mListener;

    //log tag
    public static final String TAG = "ContactListFragment.TAG";

    //factory method
    public static ContactListFragment newInstance() {
        return new ContactListFragment();
    }

    //interface listener to load contacts from main activity
    public interface ContactsListListener{
        ArrayList<Contact> loadContacts();
        void selectedContact(int position);
    }

    //method to get contacts from the interface
    private ArrayList<Contact> getContacts(){
        return mListener.loadContacts();
    }

    //method to send the selected contact index to the mainActivity
    private void sendSelectedContact(int position){
        mListener.selectedContact(position);
    }

    //onAttach method
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof ContactsListListener){
            mListener = (ContactsListListener) context;
        }
    }

    //onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    //onActivityCreated
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //getting the contacts from the main activity
        ArrayList<Contact> contacts = getContacts();

        //setting the list adapter if the contacts list is greater than 0
        if(contacts.size() != 0){
            ListAdapter adapter = new ListAdapter(getActivity(), contacts);
            setListAdapter(adapter);
        }
    }

    //click listener for the list
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        sendSelectedContact(position);

    }

}
