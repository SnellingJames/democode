package edu.fullsail.snellingjames_ce06;

//James Snelling
//AID - 201809
//MainActivity.java

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import java.util.ArrayList;
import edu.fullsail.snellingjames_ce06.fragments.ContactListFragment;
import edu.fullsail.snellingjames_ce06.fragments.DetailsFragment;
import edu.fullsail.snellingjames_ce06.fragments.NewContactFragment;

public class MainActivity extends AppCompatActivity implements NewContactFragment.NewContactListener, ContactListFragment.ContactsListListener, DetailsFragment.DetailsListener {

    //variable to hold created contacts
    private final ArrayList<Contact> contactsList = new ArrayList<>();

    //interface for the creation of new contacts
    private NewContactFragment.NewContactListener newListener;

    //variable for floating action button
    private FloatingActionButton fab;

    //variable for holding the details about the user selected contact
    private ArrayList<String> selectedContactDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //setting the context
        Context mContext = this;

        //initializing the selected contact details arrayList
        selectedContactDetails = new ArrayList<>();

        //setting the interface to the context
        newListener = (NewContactFragment.NewContactListener) mContext;

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newListener.openNewContact();
            }
        });

        //loading the contacts list or the empty stub
        getFragmentManager().beginTransaction()
                .add(R.id.fragment_container, ContactListFragment.newInstance(), ContactListFragment.TAG)
                .commit();
    }

    //interface method for opening the new contact fragment
    @Override
    public void openNewContact() {
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.slide_in_left, 0, 0, R.animator.slide_out_right)
                .replace(R.id.fragment_container, NewContactFragment.newInstance(), NewContactFragment.TAG)
                .addToBackStack("New Contact")
                .commit();

        //setting the floating action button to invisible
        fab.setVisibility(View.INVISIBLE);
    }

    //interface method for closing the new contact fragment
    @Override
    public void closeNewContact() {

        //setting the floating action button to visible
        if (fab.getVisibility() == View.INVISIBLE){
            fab.setVisibility(View.VISIBLE);
        }

        getFragmentManager().popBackStack();
    }

    //interface method for passing new contact to the contacts list
    @Override
    public void passNewContact(Contact data) {
        contactsList.add(data);
    }

    //interface method for passing the entire contacts list to the contact list fragment
    @Override
    public ArrayList<Contact> loadContacts() {
        return contactsList;
    }

    //interface method for sending the selected contact details and opening the details fragment
    @Override
    public void selectedContact(int position) {

        selectedContactDetails.clear();
        Contact selected = contactsList.get(position);
        selectedContactDetails.add(selected.getFirstName());
        selectedContactDetails.add(selected.getLastName());
        selectedContactDetails.add(selected.getPhoneNumber());
        openDetails();
    }

    //interface method for opening the details fragment
    private void openDetails() {
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.slide_in_left, 0, 0, R.animator.slide_out_right)
                .replace(R.id.fragment_container, DetailsFragment.newInstance(), DetailsFragment.TAG)
                .addToBackStack("Details")
                .commit();

        //setting the floating action button to invisible
        fab.setVisibility(View.INVISIBLE);
    }

    //interface method for closing the details fragment
    @Override
    public void closeDetails() {
        //setting the floating action button to visible
        if (fab.getVisibility() == View.INVISIBLE){
            fab.setVisibility(View.VISIBLE);
        }

        getFragmentManager().popBackStack();
    }

    //interface method for getting the selected contact to the details fragment
    @Override
    public ArrayList<String> getSelectedContact() {
        return selectedContactDetails;
    }
}
