package edu.fullsail.snellingjames_ce06.fragments;

//James Snelling
//AID - 201809
//DetailsFragment.java

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;
import edu.fullsail.snellingjames_ce06.R;

public class DetailsFragment extends Fragment {

    //variable for interface listener
    private DetailsListener mListener;

    //log tag
    public static final String TAG = "DetailsFragment.TAG";

    //factory method
    public static DetailsFragment newInstance() {

        return new DetailsFragment();
    }

    //interface to get selected contact and open the details fragment from the contact list
    public interface DetailsListener{
        void closeDetails();
        ArrayList<String> getSelectedContact();
    }

    //method for getting the selected contact
    private ArrayList<String> loadContact(){
        return mListener.getSelectedContact();
    }

    //onAttach
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof DetailsListener){
            mListener = (DetailsListener) context;
        }
    }

    //onCreateView
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.details_fragment, container, false);
    }

    //onActivityCreated
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<String> selected = loadContact();

        //getting the textViews from the fragment layout
        TextView firstNameTextView = Objects.requireNonNull(getView()).findViewById(R.id.firstNameTextView);
        TextView lastNameTextView = getView().findViewById(R.id.lastNameTextView);
        TextView phoneNumberTextView = getView().findViewById(R.id.phoneNumberTextView);

        //setting the text from the selected contact
        firstNameTextView.setText(selected.get(0));
        lastNameTextView.setText(selected.get(1));
        phoneNumberTextView.setText(selected.get(2));

        //getting the back button from the layout and setting the onClickListener to close the details
        Button backButton = getView().findViewById(R.id.backButton);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.closeDetails();
            }
        });
    }
}
