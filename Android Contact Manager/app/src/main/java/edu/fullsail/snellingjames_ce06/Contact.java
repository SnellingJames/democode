package edu.fullsail.snellingjames_ce06;

//James Snelling
//AID - 201809
//Contact.java

import java.io.Serializable;

public class Contact implements Serializable {

    //member variables for the contact object
    private final String firstName;
    private final String lastName;
    private final String phoneNumber;

    //constructor method for a new contact
    public Contact(String firstName, String lastName, String phoneNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }

    //getter for the full name
    public String getFullName(){
        return firstName + " " + lastName;
    }

    //getter for the phone number
    public String getPhoneNumber(){
        return phoneNumber;
    }

    //getter for the first name
    public String getFirstName(){
        return firstName;
    }

    //getter for the last name
    public String getLastName(){
        return lastName;
    }

}
