package edu.fullsail.snellingjames_ce06.fragments;

//James Snelling
//AID - 201809
//NewContactFragment.java

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.Objects;
import edu.fullsail.snellingjames_ce06.Contact;
import edu.fullsail.snellingjames_ce06.R;

public class NewContactFragment extends Fragment implements View.OnClickListener {

    //variable for interface
    private NewContactListener mListener;

    //variables for EditText fields
    private EditText firstName;
    private EditText lastName;
    private EditText phone;

    //log tag
    public static final String TAG = "NewContactFragment.TAG";

    //interface for passing of data and opening/closing of the fragment
    public interface NewContactListener {
        void openNewContact();
        void closeNewContact();
        void passNewContact(Contact data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof NewContactListener){
            mListener = (NewContactListener) context;
        }
    }

    //factory method
    public static NewContactFragment newInstance() {
        return new NewContactFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_contact_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button addButton = Objects.requireNonNull(getView()).findViewById(R.id.addButton);
        firstName = getView().findViewById(R.id.firstNameEditText);
        lastName = getView().findViewById(R.id.lastNameEditText);
        phone = getView().findViewById(R.id.phoneNumberEditText);

        addButton.setOnClickListener(this);
    }

    //method to pass newly created contact to MainActivity
    private void passData(Contact data){
        mListener.passNewContact(data);
    }

    //clickListener
    @Override
    public void onClick(View view) {

        if (firstName.getText().toString().isEmpty() || lastName.getText().toString().isEmpty() || phone.getText().toString().isEmpty()){
            String message = "Please do not leave any fields blank.";
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        } else {
            String first = firstName.getText().toString();
            String last = lastName.getText().toString();
            String phoneNumber = phone.getText().toString();

            Contact newContact = new Contact(first, last, phoneNumber);
            passData(newContact);
            mListener.closeNewContact();
        }

    }

}
