//James Snelling
//JAV 2 - 201811
//AddFragment.java

package edu.fullsail.snellingjames_ce02.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

import edu.fullsail.snellingjames_ce02.DatabaseHelper;
import edu.fullsail.snellingjames_ce02.R;

public class AddFragment extends Fragment {

    public static final String TAG = "AddFragment.TAG";

    private DatabaseHelper helper;

    //getting the edit text views
    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText employeeNumberEditText;
    private EditText employeeStatusEditText;
    private EditText monthEditText;
    private EditText dayEditText;
    private EditText yearEditText;

    public static final String EXTRA_ADD = "edu.fullsail.snellingjames_ce02.EXTRA_ADD";

    private int mAdd = 1;

    private int id;

    private Intent starter;

    public static AddFragment newInstance() {

        return new AddFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        helper = DatabaseHelper.getInstance(getContext());

        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(starter.hasExtra(ViewFragment.EDIT_EMPLOYEE_REQUEST)){

            //update record if this was to edit
            String firstName = firstNameEditText.getText().toString();
            String lastName = lastNameEditText.getText().toString();
            int empNumber = Integer.parseInt(employeeNumberEditText.getText().toString());
            String empStatus = employeeStatusEditText.getText().toString();
            String hireDate = yearEditText.getText().toString() + "-" + monthEditText.getText().toString() + "-" + dayEditText.getText().toString();

            helper.updateEmployee(firstName, lastName, empNumber, empStatus, hireDate, id);

            Intent resultIntent = new Intent();
            resultIntent.putExtra(EXTRA_ADD, mAdd);
            Objects.requireNonNull(getActivity()).setResult(Activity.RESULT_OK, resultIntent);

            Toast.makeText(getContext(), R.string.updated, Toast.LENGTH_SHORT).show();

            getActivity().finish();
            return super.onOptionsItemSelected(item);

        } else {

            //add record if this is a new record
            String firstName = firstNameEditText.getText().toString();
            String lastName = lastNameEditText.getText().toString();
            int empNumber = Integer.parseInt(employeeNumberEditText.getText().toString());
            String empStatus = employeeStatusEditText.getText().toString();
            String hireDate = yearEditText.getText().toString() + "-" + monthEditText.getText().toString() + "-" + dayEditText.getText().toString();

            helper.insertEmployee(firstName, lastName, empNumber, empStatus, hireDate);

            Intent resultIntent = new Intent();
            resultIntent.putExtra(EXTRA_ADD, mAdd);
            Objects.requireNonNull(getActivity()).setResult(Activity.RESULT_OK, resultIntent);

            Toast.makeText(getContext(), R.string.saved, Toast.LENGTH_SHORT).show();

            getActivity().finish();
            return super.onOptionsItemSelected(item);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //hooking up the edit text views
        firstNameEditText = Objects.requireNonNull(getView()).findViewById(R.id.firstNameEditText);
        lastNameEditText = getView().findViewById(R.id.lastNameEditText);
        employeeNumberEditText = getView().findViewById(R.id.employeeNumberEditText);
        employeeStatusEditText = getView().findViewById(R.id.employeeStatusEditText);
        monthEditText = getView().findViewById(R.id.monthEditText);
        dayEditText = getView().findViewById(R.id.dayEditText);
        yearEditText = getView().findViewById(R.id.yearEditText);

        starter = Objects.requireNonNull(getActivity()).getIntent();

        if(starter != null){
            if(starter.hasExtra(ViewFragment.EDIT_EMPLOYEE_REQUEST)){

                //filling in the editTexts with the appropriate info if this is to edit
                firstNameEditText.setText(starter.getStringExtra(ViewFragment.EXTRA_FIRST_NAME));
                lastNameEditText.setText(starter.getStringExtra(ViewFragment.EXTRA_LAST_NAME));
                employeeNumberEditText.setText(String.valueOf(starter.getIntExtra(ViewFragment.EXTRA_EMPLOYEE_NUMBER, 1)));
                employeeStatusEditText.setText(starter.getStringExtra(ViewFragment.EXTRA_EMPLOYEE_STATUS));
                String date = starter.getStringExtra(ViewFragment.EXTRA_HIRE_DATE);
                String[] dateSplit = date.split("-");
                yearEditText.setText(dateSplit[0]);
                monthEditText.setText(dateSplit[1]);
                dayEditText.setText(dateSplit[2]);
                id = starter.getIntExtra(ViewFragment.EXTRA_ID, 1);
            } else {
                mAdd = starter.getIntExtra(EXTRA_ADD, 1);
            }
        }
    }


}
