//James Snelling
//JAV 2 - 201811
//DisplayListFragment.java

package edu.fullsail.snellingjames_ce02.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Objects;

import edu.fullsail.snellingjames_ce02.AddActivity;
import edu.fullsail.snellingjames_ce02.DatabaseHelper;
import edu.fullsail.snellingjames_ce02.R;
import edu.fullsail.snellingjames_ce02.SettingActivity;
import edu.fullsail.snellingjames_ce02.ViewActivity;

public class DisplayListFragment extends ListFragment {

    public static final String TAG = "DisplayListFragment.TAG";

    public static final String EXTRA_ID = "edu.fullsail.snellingjames_ce02.ID";
    public static final String EXTRA_FIRST_NAME = "edu.fullsail.snellingjames_ce02.FIRST_NAME";
    public static final String EXTRA_LAST_NAME = "edu.fullsail.snellingjames_ce02.LAST_NAME";
    public static final String EXTRA_EMPLOYEE_NUMBER = "edu.fullsail.snellingjames_ce02.EMPLOYEE_NUMBER";
    public static final String EXTRA_EMPLOYEE_STATUS = "edu.fullsail.snellingjames_ce02.EMPLOYEE_STATUS";
    public static final String EXTRA_HIRE_DATE = "edu.fullsail.snellingjames_ce02.HIRE_DATE";

    private static final int ADD_EMPLOYEE_REQUEST = 1;
    private static final int DELETE_EMPLOYEE_REQUEST = -1;

    //database helper instance
    private DatabaseHelper helper;

    private EmployeeAdapter adapter;

    private Spinner orderSpinner;

    private Spinner sortSpinner;

    public static DisplayListFragment newInstance() {

        return new DisplayListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupFragment();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    private void setupFragment(){
        //instance of the database
        helper = DatabaseHelper.getInstance(getContext());

        adapter = new EmployeeAdapter(getContext(), helper.getAllData());

        setListAdapter(adapter);

        //setting the options menu
        setHasOptionsMenu(true);
    }

    private void sortFragment(String order, String sort){
        //instance of the database
        helper = DatabaseHelper.getInstance(getContext());

        adapter = new EmployeeAdapter(getContext(), helper.getAllDataSorted(order, sort));

        setListAdapter(adapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //setting the list adapter
        setListAdapter(new EmployeeAdapter(getContext(), helper.getAllData()));

        //hooking up the spinners and setting the click listeners
        sortSpinner = Objects.requireNonNull(getView()).findViewById(R.id.sortSpinner);
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String order;
                String sort;
                if (orderSpinner.getSelectedItem().toString().equals("Sort by Status")){
                    order = DatabaseHelper.COLUMN_EMPLOYEE_STATUS;
                } else {
                    order = DatabaseHelper.COLUMN_EMPLOYEE_NUMBER;
                }

                if (sortSpinner.getSelectedItem().toString().equals("Ascending")){
                    sort = "ASC";
                } else {
                    sort = "DESC";
                }

                sortFragment(order, sort);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        orderSpinner = getView().findViewById(R.id.orderSpinner);
        orderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String order;
                String sort;
                if (orderSpinner.getSelectedItem().toString().equals("Sort by Status")){
                    order = DatabaseHelper.COLUMN_EMPLOYEE_STATUS;
                } else {
                    order = DatabaseHelper.COLUMN_EMPLOYEE_NUMBER;
                }

                if (sortSpinner.getSelectedItem().toString().equals("Ascending")){
                    sort = "ASC";
                } else {
                    sort = "DESC";
                }

                sortFragment(order, sort);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        //inflating the menu
        inflater.inflate(R.menu.main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //opening the correct activity based on the which menu item is selected
        if (item.getItemId() == R.id.addButton){

            Intent intent1 = new Intent(getContext(), AddActivity.class);
            intent1.putExtra(AddFragment.EXTRA_ADD, 1);
            this.startActivityForResult(intent1, ADD_EMPLOYEE_REQUEST);

        } else if (item.getItemId() == R.id.settings){

            Intent intent2 = new Intent(getContext(), SettingActivity.class);
            Objects.requireNonNull(getContext()).startActivity(intent2);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(getContext(), ViewActivity.class);
        intent.putExtra(ViewFragment.EXTRA_DELETE, position);
        intent.putExtra("id", String.valueOf(id));

        Cursor c = (Cursor) this.getListView().getAdapter().getItem(position);

        intent.putExtra(EXTRA_ID, c.getInt(0));
        intent.putExtra(EXTRA_FIRST_NAME, c.getString(1));
        intent.putExtra(EXTRA_LAST_NAME, c.getString(2));
        intent.putExtra(EXTRA_EMPLOYEE_NUMBER, c.getInt(3));
        intent.putExtra(EXTRA_EMPLOYEE_STATUS, c.getString(4));
        intent.putExtra(EXTRA_HIRE_DATE, c.getString(5));

        this.startActivityForResult(intent, DELETE_EMPLOYEE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        setupFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupFragment();
    }

    //extending the ResourceCursorAdapter
    private class EmployeeAdapter extends ResourceCursorAdapter {

        //requires a cursor
        EmployeeAdapter(Context context, Cursor cursor) {
            super(context, android.R.layout.simple_list_item_2, cursor, 0);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView tv = view.findViewById(android.R.id.text1);

            //setting the text to the Column Title Data
            tv.setText(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_FIRST_NAME)));

            tv = view.findViewById(android.R.id.text2);

            //setting the text to the Column Description data
            tv.setText(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_LAST_NAME)));
        }
    }
}
