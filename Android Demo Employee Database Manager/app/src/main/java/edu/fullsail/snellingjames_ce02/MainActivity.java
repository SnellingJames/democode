//James Snelling
//JAV 2 - 201811
//MainActivity.java

package edu.fullsail.snellingjames_ce02;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import edu.fullsail.snellingjames_ce02.fragments.DisplayListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragmentContainer, DisplayListFragment.newInstance(), DisplayListFragment.TAG)
                .commit();
    }
}
