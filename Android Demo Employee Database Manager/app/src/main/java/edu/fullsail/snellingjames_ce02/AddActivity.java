//James Snelling
//JAV 2 - 201811
//AddActivity.java

package edu.fullsail.snellingjames_ce02;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import edu.fullsail.snellingjames_ce02.fragments.AddFragment;

public class AddActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.addFragmentContainer, AddFragment.newInstance(), AddFragment.TAG)
                .commit();

    }
}
