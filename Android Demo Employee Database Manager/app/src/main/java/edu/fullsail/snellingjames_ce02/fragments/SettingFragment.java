//James Snelling
//JAV 2 - 201811
//SettingFragment.java

package edu.fullsail.snellingjames_ce02.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

import edu.fullsail.snellingjames_ce02.DatabaseHelper;
import edu.fullsail.snellingjames_ce02.R;

public class SettingFragment extends PreferenceFragmentCompat {

    //instance of the databaseHelper
    private DatabaseHelper helper;

    //loading the preferences from the xml file
    @Override
    public void onCreatePreferences(Bundle bundle, String s) {

        setPreferencesFromResource(R.xml.preference_fragment, s);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //getting the instance of the database
        helper = DatabaseHelper.getInstance(getContext());

        //hooking up the preferences
        android.support.v7.preference.Preference datePref = findPreference("PREF_LIST");
        android.support.v7.preference.Preference dataPref = findPreference("PREF_CLICK");

        //setting the listeners for the delete all preferences
        dataPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                //building the dialog to warn the user and setup delete all functionality
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.delete_all);
                builder.setMessage(R.string.delete_all_message);
                builder.setNegativeButton(R.string.cancel, null);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        helper.getWritableDatabase().delete("employees", null, null);
                    }
                });

                builder.show();
                return false;
            }
        });
    }
}
