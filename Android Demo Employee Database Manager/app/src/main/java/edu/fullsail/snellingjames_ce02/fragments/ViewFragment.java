//James Snelling
//JAV 2 - 201811
//ViewFragment.java

package edu.fullsail.snellingjames_ce02.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import edu.fullsail.snellingjames_ce02.AddActivity;
import edu.fullsail.snellingjames_ce02.DatabaseHelper;
import edu.fullsail.snellingjames_ce02.R;

public class ViewFragment extends Fragment {

    public static final String TAG = "ViewFragment.TAG";
    public static final String EXTRA_DELETE = "edu.fullsail.snellingjames_ce02.EXTRA_DELETE";
    private static final String EXTRA_DELETE_ID = "edu.fullsail.snellingjames_ce02.DELETE_ID";

    public static final String EDIT_EMPLOYEE_REQUEST = "edu.fullsail.snellingjames_ce02.EMPLOYEE_REQUEST";

    public static final String EXTRA_ID = "edu.fullsail.snellingjames_ce02.ID";
    public static final String EXTRA_FIRST_NAME = "edu.fullsail.snellingjames_ce02.FIRST_NAME";
    public static final String EXTRA_LAST_NAME = "edu.fullsail.snellingjames_ce02.LAST_NAME";
    public static final String EXTRA_EMPLOYEE_NUMBER = "edu.fullsail.snellingjames_ce02.EMPLOYEE_NUMBER";
    public static final String EXTRA_EMPLOYEE_STATUS = "edu.fullsail.snellingjames_ce02.EMPLOYEE_STATUS";
    public static final String EXTRA_HIRE_DATE = "edu.fullsail.snellingjames_ce02.HIRE_DATE";

    private final int mDelete = -1;
    private int id;

    private DatabaseHelper helper;

    private Intent starter;

    public static ViewFragment newInstance() {

        return new ViewFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        helper = DatabaseHelper.getInstance(getContext());

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.view_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.editEmployeeButton){

            Intent intent1 = new Intent(getContext(), AddActivity.class);
            intent1.putExtra(EDIT_EMPLOYEE_REQUEST, "Edit");
            intent1.putExtra(EXTRA_ID, starter.getIntExtra(DisplayListFragment.EXTRA_ID, 1));
            intent1.putExtra(EXTRA_FIRST_NAME, starter.getStringExtra(DisplayListFragment.EXTRA_FIRST_NAME));
            intent1.putExtra(EXTRA_LAST_NAME, starter.getStringExtra(DisplayListFragment.EXTRA_LAST_NAME));
            intent1.putExtra(EXTRA_EMPLOYEE_NUMBER, starter.getIntExtra(DisplayListFragment.EXTRA_EMPLOYEE_NUMBER, 1));
            intent1.putExtra(EXTRA_EMPLOYEE_STATUS, starter.getStringExtra(DisplayListFragment.EXTRA_EMPLOYEE_STATUS));
            intent1.putExtra(EXTRA_HIRE_DATE, starter.getStringExtra(DisplayListFragment.EXTRA_HIRE_DATE));

            Objects.requireNonNull(getContext()).startActivity(intent1);

            Objects.requireNonNull(getActivity()).finish();

        } else if(item.getItemId() == R.id.deleteEmployeeButton){

            AlertDialog.Builder deleteAlert = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
            deleteAlert.setTitle(R.string.delete_employee);
            deleteAlert.setMessage(R.string.are_you_sure);
            deleteAlert.setNegativeButton(R.string.cancel, null);
            deleteAlert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    helper.getWritableDatabase().delete("employees", "_ID = " + id, null);

                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(EXTRA_DELETE, mDelete);
                    resultIntent.putExtra(EXTRA_DELETE_ID, id);
                    Objects.requireNonNull(getActivity()).setResult(Activity.RESULT_OK, resultIntent);


                    Toast.makeText(getContext(), R.string.deleted, Toast.LENGTH_SHORT).show();

                    getActivity().finish();
                }
            });

            deleteAlert.show();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SharedPreferences defaultPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        System.out.println(defaultPrefs.getAll());
        String listString = defaultPrefs.getString("PREF_LIST", "");
        System.out.println(listString);

        TextView firstName = Objects.requireNonNull(getView()).findViewById(R.id.firstNameTextView);
        TextView lastName = getView().findViewById(R.id.lastNameTextView);
        TextView empNumber = getView().findViewById(R.id.employeeNumberTextView);
        TextView empStatus = getView().findViewById(R.id.employeeStatusTextView);
        TextView hireDate = getView().findViewById(R.id.hireDateTextView);

        starter = Objects.requireNonNull(getActivity()).getIntent();

        if(starter != null){

            if(listString != null){
                String date = starter.getStringExtra(DisplayListFragment.EXTRA_HIRE_DATE);
                Date dateFormat;

                switch (listString){
                    case "1":
                        date = date.replaceAll("-", "/");
                        hireDate.setText(date);
                        break;
                    case "2":
                        try{
                            dateFormat = new SimpleDateFormat("yyyy-mm-dd", Locale.US).parse(date);
                            String formattedDate = new SimpleDateFormat("MMMM dd, yyyy", Locale.US).format(dateFormat);
                            hireDate.setText(formattedDate);
                        } catch (ParseException e){
                            System.out.println(e);
                        }

                        break;
                    case "3":
                        date = date.replaceAll("-", ".");
                        hireDate.setText(date);
                        break;

                }
            }
            starter.getIntExtra(EXTRA_DELETE, -1);
            firstName.setText(starter.getStringExtra(DisplayListFragment.EXTRA_FIRST_NAME));
            lastName.setText(starter.getStringExtra(DisplayListFragment.EXTRA_LAST_NAME));
            empNumber.setText(String.valueOf(starter.getIntExtra(DisplayListFragment.EXTRA_EMPLOYEE_NUMBER, 1)));
            empStatus.setText(starter.getStringExtra(DisplayListFragment.EXTRA_EMPLOYEE_STATUS));
            id = starter.getIntExtra(DisplayListFragment.EXTRA_ID, 1);
        }
    }
}
