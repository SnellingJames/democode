//James Snelling
//JAV 2 - 201811
//DatabaseHelper.java

package edu.fullsail.snellingjames_ce02;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    //variables to hold database name and version for constructor
    private static final String DATABASE_FILE = "database.db";
    private static final int DATABASE_VERSION = 1;

    //variables to create database creation string
    private static final String TABLE_NAME = "employees";
    private static final String COLUMN_ID = "_id";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_EMPLOYEE_NUMBER = "employee_number";
    public static final String COLUMN_EMPLOYEE_STATUS = "employee_status";
    private static final String COLUMN_HIRE_DATE = "hire_date";

    //database creation string
    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_FIRST_NAME + " TEXT, " +
            COLUMN_LAST_NAME + " TEXT, " +
            COLUMN_EMPLOYEE_NUMBER + " TEXT, " +
            COLUMN_EMPLOYEE_STATUS + " TEXT, " +
            COLUMN_HIRE_DATE + " DATETIME) ";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    //private database helper instance
    private static DatabaseHelper INSTANCE = null;

    //public method to get database instance. keeps database instance as a singleton
    public static DatabaseHelper getInstance(Context context){

        //if it hasn't been created, create it, otherwise return the instance
        if (INSTANCE == null){
            INSTANCE = new DatabaseHelper(context);
        }

        return INSTANCE;
    }

    //private variable to hold the database instance
    private final SQLiteDatabase mDatabase;

    //private constructor to create the single instance of the database
    private DatabaseHelper(Context context){
        super(context, DATABASE_FILE, null, DATABASE_VERSION);

        mDatabase = getWritableDatabase();
    }

    public long updateEmployee(String firstName, String lastName, int employeeNumber, String employeeStatus, String hireDate, int id){

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_FIRST_NAME, firstName);
        contentValues.put(COLUMN_LAST_NAME, lastName);
        contentValues.put(COLUMN_EMPLOYEE_NUMBER, employeeNumber);
        contentValues.put(COLUMN_EMPLOYEE_STATUS, employeeStatus);
        contentValues.put(COLUMN_HIRE_DATE, hireDate);

        return mDatabase.update("employees", contentValues, "_ID = " + id, null);
    }
    //public method to add to the database
    public long insertEmployee(String firstName, String lastName, int employeeNumber, String employeeStatus, String hireDate ){

        //object to put new article details into the database
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_FIRST_NAME, firstName);
        contentValues.put(COLUMN_LAST_NAME, lastName);
        contentValues.put(COLUMN_EMPLOYEE_NUMBER, employeeNumber);
        contentValues.put(COLUMN_EMPLOYEE_STATUS, employeeStatus);
        contentValues.put(COLUMN_HIRE_DATE, hireDate);

        //inserts new article details using table name, nullColumnHack (whether or not a column can accept a null value), and the contentValues variable
        //returns the new row number created
        return mDatabase.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getAllData(){
        return mDatabase.query(TABLE_NAME, null, null, null, null, null, null);
    }

    public Cursor getAllDataSorted(String orderBy, String ascOrDesc){
        return mDatabase.query(TABLE_NAME, null, null, null, null, null, orderBy + " " + ascOrDesc);
    }
}
