//James Snelling
//JAV 2 - 201811
//ViewActivity.java

package edu.fullsail.snellingjames_ce02;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import edu.fullsail.snellingjames_ce02.fragments.ViewFragment;

public class ViewActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.viewFragmentContainer, ViewFragment.newInstance(), ViewFragment.TAG)
                .commit();
    }
}
