package edu.fullsail.snellingjames_ce05;

//James Snelling
//AID - 201809
//Teacher.java

public class Teacher extends Person {

    //constructor
    public Teacher(String firstName, String lastName, String course){
        this.firstName = firstName;
        this.lastName = lastName;
        this.extraVariable = course;
    }

}
