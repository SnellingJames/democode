package edu.fullsail.snellingjames_ce05.fragments;

//James Snelling
//AID - 201809
//StudentFormFragment.java

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

import edu.fullsail.snellingjames_ce05.R;
import edu.fullsail.snellingjames_ce05.Student;

public class StudentFormFragment extends Fragment{

    //log tag
    public static final String TAG = "StudentFormFragment.TAG";

    //interface
    private StudentListener mListener;

    //factory method
    public static StudentFormFragment newInstance() {
        return new StudentFormFragment();
    }

    //interface for passing data and fragment control
    public interface StudentListener{
        void passNewStudent(Student data);
    }

    private void passData(Student data){
        mListener.passNewStudent(data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof StudentListener){
            mListener = (StudentListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.student_form_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //getting the EditText views
        final EditText firstNameEditText = Objects.requireNonNull(getView()).findViewById(R.id.firstNameEditText);
        final EditText lastNameEditText = getView().findViewById(R.id.lastNameEditText);
        final EditText idEditText = getView().findViewById(R.id.idEditText);
        Button addButton = getView().findViewById(R.id.addButton);

        //setting the listener for the add button
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //if the EditText fields are empty, notify the user
                if (firstNameEditText.getText().toString().isEmpty() || lastNameEditText.getText().toString().isEmpty() || idEditText.getText().toString().isEmpty()){
                    String message = "Please do not leave any fields blank.";
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } else {

                    //getting the user entered data
                    String first = firstNameEditText.getText().toString();
                    String last = lastNameEditText.getText().toString();
                    String id = idEditText.getText().toString();

                    //creating a new object based on the input
                    Student newStudent = new Student(first, last, id);

                    //sending the new object to the MainActivity
                    passData(newStudent);

                    //clearing the fields
                    firstNameEditText.clearComposingText();
                    lastNameEditText.clearComposingText();
                    idEditText.clearComposingText();
                }
            }
        });
    }

}
