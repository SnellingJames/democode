package edu.fullsail.snellingjames_ce05.fragments;

//James Snelling
//AID - 201809
//TeacherFormFragment.java

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

import edu.fullsail.snellingjames_ce05.R;
import edu.fullsail.snellingjames_ce05.Teacher;

public class TeacherFormFragment extends Fragment{

    //log tag
    public static final String TAG = "TeacherFormFragment.TAG";

    //interface
    private TeacherListener mListener;

    //factory method
    public static TeacherFormFragment newInstance() {
        return new TeacherFormFragment();
    }

    //interface for passing data and fragment control
    public interface TeacherListener{
        void passNewTeacher(Teacher data);
    }

    private void passData(Teacher data){
        mListener.passNewTeacher(data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof TeacherListener){
            mListener = (TeacherListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.teacher_form_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final EditText firstNameEditText = Objects.requireNonNull(getView()).findViewById(R.id.firstNameEditText);
        final EditText lastNameEditText = getView().findViewById(R.id.lastNameEditText);
        final EditText courseEditText = getView().findViewById(R.id.courseEditText);
        Button addButton = getView().findViewById(R.id.addButton);

        //setting the click listener for the add button
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //if the EditText fields are empty, notify the user
                if (firstNameEditText.getText().toString().isEmpty() || lastNameEditText.getText().toString().isEmpty() || courseEditText.getText().toString().isEmpty()){
                    String message = "Please do not leave any fields blank.";
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } else {

                    //getting the user entered data
                    String first = firstNameEditText.getText().toString();
                    String last = lastNameEditText.getText().toString();
                    String course = courseEditText.getText().toString();

                    //creating a new object based on the input
                    Teacher newTeacher = new Teacher(first, last, course);

                    //sending the new object to the MainActivity
                    passData(newTeacher);

                    //clearing the fields
                    firstNameEditText.clearComposingText();
                    lastNameEditText.clearComposingText();
                    courseEditText.clearComposingText();
                }
            }
        });
    }
}
