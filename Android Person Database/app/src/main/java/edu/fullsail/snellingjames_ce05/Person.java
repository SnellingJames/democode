package edu.fullsail.snellingjames_ce05;

//James Snelling
//AID - 201809
//Person.java

class Person {

    //member variables
    String firstName;
    String lastName;
    String extraVariable;

    //default constructor
    Person(){

    }

    //member variables getter
    private String getFullName(){
        return firstName + " " + lastName;
    }

    //getting the additional variable that could be course, id, or program
    private String getExtraVariable(){
        return extraVariable;
    }

    //toString method
    public String toString(){
        return getFullName() + " | " + getExtraVariable();
    }
}
