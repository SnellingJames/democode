package edu.fullsail.snellingjames_ce05;

//James Snelling
//AID - 201809
//Student.java

public class Student extends Person {

    //constructor
    public Student(String firstName, String lastName, String id){
        this.firstName = firstName;
        this.lastName = lastName;
        this.extraVariable = id;
    }

}
