package edu.fullsail.snellingjames_ce05;

//James Snelling
//AID - 201809
//Administrator.java

public class Administrator extends Person {

    //constructor
    public Administrator(String firstName, String lastName, String program){
        this.firstName = firstName;
        this.lastName = lastName;
        this.extraVariable = program;
    }

}
