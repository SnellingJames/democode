package edu.fullsail.snellingjames_ce05;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class PersonListAdapter extends BaseAdapter {
    private final Context mContext;
    private final ArrayList<String> mArrayList;
    private static final int ID_CONSTANT = 0x01000000;


    public PersonListAdapter(Context c, ArrayList<String> objects) {

        mContext = c;
        mArrayList = objects;
    }


    @Override
    public int getCount(){
        if (mArrayList != null){
            return mArrayList.size();
        } else {
            return 0;
        }
    }

    @Override
    public long getItemId(int position){
        return ID_CONSTANT + position;
    }

    @Override
    public String getItem(int position){
        if (mArrayList != null && position<mArrayList.size() && position>=0){
            return mArrayList.get(position);
        } else {
            return null;
        }
    }

    public View getView(int position, View convertView, ViewGroup parent){

        //variable for ViewHolder
        ListViewHolder viewHolder;

        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item, parent, false);

            //creating a new ViewHolder to hold the convertView fields
            viewHolder = new ListViewHolder();
            viewHolder.infoTextView = convertView.findViewById(R.id.infoTextView);

            //setting the convertView's tag as the newly created ViewHolder
            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ListViewHolder) convertView.getTag();

        }

        String item = getItem(position);

        if (item != null){

            viewHolder.infoTextView.setText(item);
        }

        return convertView;
    }

    //static class for the ViewHolder
    static class ListViewHolder{
        TextView infoTextView;
    }
}
