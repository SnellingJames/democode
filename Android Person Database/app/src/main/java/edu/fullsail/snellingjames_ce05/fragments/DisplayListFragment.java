package edu.fullsail.snellingjames_ce05.fragments;

//James Snelling
//AID - 201809
//DisplayListFragment.java

import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import edu.fullsail.snellingjames_ce05.PersonListAdapter;
import edu.fullsail.snellingjames_ce05.R;

public class DisplayListFragment extends ListFragment {


    //log tag
    public static final String TAG = "DisplayListFragment.TAG";

    //interface
    private DisplayListener mListener;

    //factory method
    public static DisplayListFragment newInstance() {
        return new DisplayListFragment();
    }

    //interface for getting the store objects to display
    public interface DisplayListener{
        ArrayList<String> loadSaved();
    }

    //method for getting the saved person objects
    private ArrayList<String> getSaved(){
        return mListener.loadSaved();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof DisplayListener){
            mListener = (DisplayListener) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.display_list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //loading the saved person objects
        ArrayList<String> loaded = getSaved();

        if (loaded.size() != 0){
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                PersonListAdapter adapter = new PersonListAdapter(getContext(), loaded);
                setListAdapter(adapter);
            }
        }
    }
}
