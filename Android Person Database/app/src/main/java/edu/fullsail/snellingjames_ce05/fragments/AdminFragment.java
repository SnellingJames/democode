package edu.fullsail.snellingjames_ce05.fragments;

//James Snelling
//AID - 201809
//AdminFragment.java

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

import edu.fullsail.snellingjames_ce05.Administrator;
import edu.fullsail.snellingjames_ce05.R;

public class AdminFragment extends Fragment {

    //log tag
    public static final String TAG = "AdminFragment.TAG";

    //interface
    private AdminListener mListener;

    //factory method
    public static AdminFragment newInstance() {
        return new AdminFragment();
    }

    //interface for passing data and fragment control
    public interface AdminListener{
        void passNewAdmin(Administrator data);
    }

    private void passData(Administrator data){
        mListener.passNewAdmin(data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof AdminListener){
            mListener = (AdminListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.admin_form_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //getting the editText views
        final EditText firstNameEditText = Objects.requireNonNull(getView()).findViewById(R.id.firstNameEditText);
        final EditText lastNameEditText = getView().findViewById(R.id.lastNameEditText);
        final EditText programEditText = getView().findViewById(R.id.programEditText);
        Button addButton = getView().findViewById(R.id.addButton);

        //setting the listener for the add button
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //if any fields are empty, notify the user
                if (firstNameEditText.getText().toString().isEmpty() || lastNameEditText.getText().toString().isEmpty() || programEditText.getText().toString().isEmpty()){
                    String message = "Please do not leave any fields blank.";
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } else {

                    //getting the user entered data
                    String first = firstNameEditText.getText().toString();
                    String last = lastNameEditText.getText().toString();
                    String id = programEditText.getText().toString();

                    //creating a new object based on the input
                    Administrator newAdmin = new Administrator(first, last, id);

                    //sending the new object to the MainActivity
                    passData(newAdmin);

                    //clearing the fields
                    firstNameEditText.clearComposingText();
                    lastNameEditText.clearComposingText();
                    programEditText.clearComposingText();
                }
            }
        });
    }

}
