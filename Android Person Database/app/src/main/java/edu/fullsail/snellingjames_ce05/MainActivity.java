package edu.fullsail.snellingjames_ce05;

//James Snelling
//AID - 201809
//MainActivity.java

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;

import java.util.ArrayList;

import edu.fullsail.snellingjames_ce05.fragments.AdminFragment;
import edu.fullsail.snellingjames_ce05.fragments.DisplayListFragment;
import edu.fullsail.snellingjames_ce05.fragments.StudentFormFragment;
import edu.fullsail.snellingjames_ce05.fragments.TeacherFormFragment;

public class MainActivity extends AppCompatActivity implements AdminFragment.AdminListener, DisplayListFragment.DisplayListener, StudentFormFragment.StudentListener, TeacherFormFragment.TeacherListener {

    //variables for storing the person objects
    private ArrayList<String> personList;

    private final String SAVE_ARRAY_LIST = "SAVE_KEY_ARRAY_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //checking if there's a savedInstanceState
        if (savedInstanceState != null){

            personList = savedInstanceState.getStringArrayList(SAVE_ARRAY_LIST);

        } else {
            //initializing the arrayList
            personList = new ArrayList<>();
        }

        //setting the spinner and changing the forms based on spinner selection
        Spinner formSpinner = findViewById(R.id.selectionSpinner);
        formSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String selectedFormType = adapterView.getItemAtPosition(i).toString();
                switch (selectedFormType){

                    case "Teacher":
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fragmentContainer, TeacherFormFragment.newInstance(), TeacherFormFragment.TAG)
                                .commit();
                        break;

                    case "Administrator":
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fragmentContainer, AdminFragment.newInstance(), AdminFragment.TAG)
                                .commit();
                        break;

                    case "Student":
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fragmentContainer, StudentFormFragment.newInstance(), StudentFormFragment.TAG)
                                .commit();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //variable to hold copy of the list frame layout
        final FrameLayout listFrame = findViewById(R.id.listContainer);

        //setting the list
        getFragmentManager().beginTransaction()
                .add(R.id.listContainer, DisplayListFragment.newInstance(), DisplayListFragment.TAG)
                .commit();

        //setting the form
        getFragmentManager().beginTransaction()
                .add(R.id.fragmentContainer, StudentFormFragment.newInstance(), StudentFormFragment.TAG)
                .commit();

        //setting the refresh button listener
        Button refreshButton = findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //removing the existing view
                listFrame.removeAllViewsInLayout();

                //adding a new version of the list
                getFragmentManager().beginTransaction()
                        .add(R.id.listContainer, DisplayListFragment.newInstance(), DisplayListFragment.TAG)
                        .commit();
            }
        });
    }

    //interface method for a new administrator
    @Override
    public void passNewAdmin(Administrator data) {

        personList.add(data.toString());
    }

    //interface method for passing the saved person objects
    @Override
    public ArrayList<String> loadSaved() {
        return personList;
    }

    //interface method for a new student
    @Override
    public void passNewStudent(Student data) {

        personList.add(data.toString());
        System.out.println(data.toString());
    }

    //interface method for a new teacher
    @Override
    public void passNewTeacher(Teacher data) {

        personList.add(data.toString());
        System.out.println(data.toString());
    }

    //saving the state
    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);

        try{

            ArrayList<String> savedStrings = personList;
            outState.putStringArrayList(SAVE_ARRAY_LIST, savedStrings);

        } catch (Exception e){

            e.printStackTrace();
        }
    }
}
