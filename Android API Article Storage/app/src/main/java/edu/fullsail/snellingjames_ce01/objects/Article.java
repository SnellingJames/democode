//James Snelling
//JAV II - 201811
//Article.java

package edu.fullsail.snellingjames_ce01.objects;

import java.io.Serializable;

public class Article implements Serializable {

    private final String title;
    private final String subtitle;
    private final int ups;

    public Article(String title, String subtitle, int ups){
        this.title = title;
        this.subtitle = subtitle;
        this.ups = ups;
    }

    public String getTitle(){
        return title;
    }

    public String getSubtitle(){
        return subtitle;
    }

    public int getUps(){
        return ups;
    }
}
