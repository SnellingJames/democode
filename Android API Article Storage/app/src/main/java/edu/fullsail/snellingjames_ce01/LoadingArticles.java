//James Snelling
//JAV II - 201811
//LoadingArticles.java

package edu.fullsail.snellingjames_ce01;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import edu.fullsail.snellingjames_ce01.objects.Article;

import static android.content.Context.MODE_PRIVATE;

class LoadingArticles extends AsyncTask<String, Void, String> {

    //variable to hold Activity context
    private final Context mContext;

    //string to hold conditional filename
    private final String filename;

    //variable to hold the downloaded articles to pass to MainActivity
    private final ArrayList<Article> resultList = new ArrayList<>();

    //local interface variable
    private LoadListener mListener;

    //interface to pass data
    public interface LoadListener{
        void passDownloadedList(ArrayList<Article> list);
    }

    //method to pass data to interface
    private void passData(ArrayList<Article> list){
        mListener.passDownloadedList(list);
    }

    //constructor takes in context to run network connectivity checks in the class
    public LoadingArticles(Context context, String filename){
        mContext = context;
        this.filename = filename;
        if(context instanceof LoadListener){
            mListener = (LoadListener) mContext;
        }
    }

    //method to get the API data
    private String getNetworkData(String urlString){
        try{

            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream is = connection.getInputStream();
            String data = IOUtils.toString(is);

            is.close();
            connection.disconnect();

            return data;

        } catch (IOException e){

            e.printStackTrace();
        }
        return null;
    }

    //method to parse the JSON data into article objects
    private void parseData(String data){

        try{
            //getting the JSON data parsed
            JSONObject articleData = new JSONObject(data);

            JSONObject articles = articleData.getJSONObject("data");

            JSONArray articleArray = articles.getJSONArray("children");

            for (int i = 0; i < articleArray.length(); i++){

                String titleString;
                String subtitleString;
                int upsInt;

                JSONObject rawArticle = articleArray.getJSONObject(i);
                JSONObject article = rawArticle.getJSONObject("data");

                titleString = article.getString("title");
                if (article.getString("selftext").equals("")){
                    subtitleString = article.getString("url");
                } else {
                    subtitleString = article.getString("selftext");
                }
                upsInt = article.getInt("ups");

                Article newArticle = new Article(titleString, subtitleString, upsInt);

                System.out.println(newArticle.getTitle());

                resultList.add(newArticle);


            }

            saveObjects();

        } catch(JSONException e) {
            e.printStackTrace();
        }
    }

    //method to save the downloaded data to the device
    private void saveObjects(){
        try {
            FileOutputStream fos = mContext.openFileOutput(filename, MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(resultList);
            oos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... strings) {

        //get the JSON data
        String data = getNetworkData(strings[0]);

        //parse the JSON data
        parseData(data);

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        passData(resultList);
    }
}
