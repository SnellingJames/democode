//James Snelling
//JAV II - 201811
//ResultListAdapter.java

package edu.fullsail.snellingjames_ce01;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import edu.fullsail.snellingjames_ce01.objects.Article;

public class ResultListAdapter extends BaseAdapter {

    private final Context mContext;
    private final ArrayList<Article> mArrayList;
    private static final int ID_CONSTANT = 0x01000000;

    public ResultListAdapter(Context context, ArrayList<Article> objects){
        mContext = context;
        mArrayList = objects;
    }

    @Override
    public int getCount(){
        if (mArrayList != null){
            return mArrayList.size();
        } else {
            return 0;
        }
    }

    @Override
    public long getItemId(int position){
        return ID_CONSTANT + position;
    }

    @Override
    public Article getItem(int position){
        if (mArrayList != null && position<mArrayList.size() && position>=0){
            return mArrayList.get(position);
        } else {
            return null;
        }
    }

    public View getView(int position, View convertView, ViewGroup parent){

        //variable for ViewHolder
        ListViewHolder viewHolder;

        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item, parent, false);

            //creating a new ViewHolder to hold the convertView fields
            viewHolder = new ListViewHolder();
            viewHolder.upsTextView = convertView.findViewById(R.id.upsTextView);
            viewHolder.subtitleTextView = convertView.findViewById(R.id.articleSubTitleTextView);
            viewHolder.titleTextView = convertView.findViewById(R.id.articleTitleTextView);

            //setting the convertView's tag as the newly created ViewHolder
            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ListViewHolder) convertView.getTag();

        }

        Article item = getItem(position);

        if (item != null){

            viewHolder.upsTextView.setText(String.valueOf(item.getUps()));
            viewHolder.titleTextView.setText(item.getTitle());
            viewHolder.subtitleTextView.setText(item.getSubtitle());
        }

        return convertView;
    }

    //static class for the ViewHolder
    static class ListViewHolder{
        TextView upsTextView;
        TextView titleTextView;
        TextView subtitleTextView;
    }

}
