//James Snelling
//JAV II - 201811
//ResultListFragment.java

package edu.fullsail.snellingjames_ce01.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import edu.fullsail.snellingjames_ce01.R;
import edu.fullsail.snellingjames_ce01.ResultListAdapter;
import edu.fullsail.snellingjames_ce01.objects.Article;

public class ResultListFragment extends ListFragment {

    public static final String TAG = "ResultListFragment.TAG";

    private ResultListener mListener;

    public interface ResultListener{
        ArrayList<Article> passDisplayList();
    }

    private ArrayList<Article> getList(){
        return mListener.passDisplayList();
    }

    public static ResultListFragment newInstance() {

        return new ResultListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof ResultListener){
            mListener = (ResultListener)context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_result_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayList<Article> articleList = getList();

        ResultListAdapter adapter = new ResultListAdapter(getContext(), articleList);
        setListAdapter(adapter);
    }
}
