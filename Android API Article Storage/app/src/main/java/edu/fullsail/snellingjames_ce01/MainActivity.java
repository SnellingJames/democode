//James Snelling
//JAV II - 201811
//MainActivity.java

package edu.fullsail.snellingjames_ce01;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import edu.fullsail.snellingjames_ce01.fragments.ResultListFragment;
import edu.fullsail.snellingjames_ce01.objects.Article;

public class MainActivity extends AppCompatActivity implements ResultListFragment.ResultListener, LoadingArticles.LoadListener {

    //variable to hold loaded list
    private ArrayList<Article> articleList = new ArrayList<>();

    //variable to hold the URL for the API request
    private String urlString;

    //variable to hold context
    private Context mContext;

    private FrameLayout listContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //setting the context to pass into the asyncTasks
        mContext = this;

        //hooking up the listContainer
        listContainer = findViewById(R.id.displayListContainer);

        //setting the listFragment to display
        getSupportFragmentManager().beginTransaction()
                .add(R.id.displayListContainer, ResultListFragment.newInstance(), ResultListFragment.TAG)
                .commit();

        //hooking up the spinner
        Spinner selectionSpinner = findViewById(R.id.categorySpinner);

        //setting up the onItemSelectedListener for the spinner
        selectionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                //clearing the article list to better refresh list
                articleList.clear();

                //getting selected item from spinner
                String selectedItem = adapterView.getItemAtPosition(i).toString();

                //boolean to show if network is connected or not
                boolean connected = checkNetwork();

                switch (selectedItem){
                    case "Geek":
                        if(connected){
                            urlString = "https://www.reddit.com/r/geek/hot.json";
                            LoadingArticles geekArticles = new LoadingArticles(mContext, "geek");
                            geekArticles.execute(urlString);
                            loadSavedArticles("geek");
                            refreshListFragment();
                        } else {
                            Toast.makeText(mContext, R.string.no_network, Toast.LENGTH_SHORT).show();
                            loadSavedArticles("geek");
                            refreshListFragment();
                            if(articleList.isEmpty()){
                                Toast.makeText(mContext, R.string.no_saved_data, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mContext, R.string.viewing_saved_data, Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;

                    case "News":
                        if(connected){
                            urlString = "https://www.reddit.com/r/news/hot.json";
                            LoadingArticles newsArticles = new LoadingArticles(mContext, "news");
                            newsArticles.execute(urlString);
                            loadSavedArticles("news");
                            refreshListFragment();
                        } else {
                            Toast.makeText(mContext, R.string.no_network, Toast.LENGTH_SHORT).show();
                            loadSavedArticles("news");
                            refreshListFragment();
                            if(articleList.isEmpty()){
                                Toast.makeText(mContext, R.string.no_saved_data, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mContext, R.string.viewing_saved_data, Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;

                    case "Technology":
                        if(connected){
                            urlString = "https://www.reddit.com/r/technology/hot.json";
                            LoadingArticles techArticles = new LoadingArticles(mContext, "tech");
                            techArticles.execute(urlString);
                            loadSavedArticles("tech");
                            refreshListFragment();
                        } else {
                            Toast.makeText(mContext, R.string.no_network, Toast.LENGTH_SHORT).show();
                            loadSavedArticles("tech");
                            refreshListFragment();
                            if(articleList.isEmpty()){
                                Toast.makeText(mContext, R.string.no_saved_data, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mContext, R.string.viewing_saved_data, Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;

                    case "Space":
                        if(connected){
                            urlString = "https://www.reddit.com/r/space/hot.json";
                            LoadingArticles spaceArticles = new LoadingArticles(mContext, "space");
                            spaceArticles.execute(urlString);
                            loadSavedArticles("space");
                            refreshListFragment();
                        } else {
                            Toast.makeText(mContext, R.string.no_network, Toast.LENGTH_SHORT).show();
                            loadSavedArticles("space");
                            refreshListFragment();
                            if(articleList.isEmpty()){
                                Toast.makeText(mContext, R.string.no_saved_data, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mContext, R.string.viewing_saved_data, Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;

                    case "Jokes":
                        if(connected){
                            urlString = "https://www.reddit.com/r/jokes/hot.json";
                            LoadingArticles jokeArticles = new LoadingArticles(mContext, "jokes");
                            jokeArticles.execute(urlString);
                            loadSavedArticles("jokes");
                            refreshListFragment();
                        } else {
                            Toast.makeText(mContext, R.string.no_network, Toast.LENGTH_SHORT).show();
                            loadSavedArticles("jokes");
                            refreshListFragment();
                            if(articleList.isEmpty()){
                                Toast.makeText(mContext, R.string.no_saved_data, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mContext, R.string.viewing_saved_data, Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private boolean checkNetwork(){
        //checking for connectivity
        ConnectivityManager mgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        //setting a default value of false
        boolean isConnected = false;

        if(mgr != null){
            NetworkInfo info = mgr.getActiveNetworkInfo();
            if(info != null){

                //setting boolean based on connection status
                isConnected = info.isConnected();
            }
        }
        return isConnected;
    }

    //method to load saved data if there's no network connection
    private void loadSavedArticles(String filename){

        try {
            FileInputStream fis = openFileInput(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);

            articleList = (ArrayList<Article>) ois.readObject();

            ois.close();
            fis.close();
            if(articleList.isEmpty()){
                Toast.makeText(mContext, R.string.no_saved_data, Toast.LENGTH_SHORT).show();
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void refreshListFragment(){
        listContainer.removeAllViewsInLayout();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.displayListContainer, ResultListFragment.newInstance(), ResultListFragment.TAG)
                .commit();
    }

    //interface method to pass downloaded article list to the ListFragment
    @Override
    public ArrayList<Article> passDisplayList() {
        return articleList;
    }

    //interface method to get downloaded article list from AsyncTask
    @Override
    public void passDownloadedList(ArrayList<Article> list) {
        articleList = list;
    }
}
