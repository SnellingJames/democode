//James Snelling
//JAV 2 - 201811
//Contact.java

package edu.fullsail.snellingjamesce_04;

import android.graphics.drawable.Drawable;

import java.io.Serializable;
import java.util.ArrayList;

public class Contact implements Serializable {

    //class variables
    private final String firstName;
    private final String lastName;
    private final String middleName;
    private final ArrayList<String> phoneNumber;
    private final String id;
    private final Drawable displayImage;

    //constructor
    public Contact(String fName, String mName, String lName, ArrayList<String> number, String id, Drawable image){
        this.firstName = fName;
        this.middleName = mName;
        this.lastName = lName;
        this.phoneNumber = number;
        this.id = id;
        this.displayImage = image;
    }

    //getters
    public String getDisplayName(){
        return firstName + " " + middleName + " " + lastName;
    }

    public String getListName(){
        return firstName + " " + lastName;
    }

    public String getId(){
        return id;
    }

    public ArrayList<String> getPhoneNumber(){
        return phoneNumber;
    }

    public Drawable getDisplayImage(){
        return displayImage;
    }
}
