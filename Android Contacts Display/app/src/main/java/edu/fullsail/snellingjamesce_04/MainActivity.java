//James Snelling
//JAV 2 - 201811
//MainActivity.java

package edu.fullsail.snellingjamesce_04;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import edu.fullsail.snellingjamesce_04.fragments.ContactListFragment;
import edu.fullsail.snellingjamesce_04.fragments.DetailsFragment;

public class MainActivity extends AppCompatActivity implements ContactListFragment.ContactListListener, DetailsFragment.DetailListener {

    //variable to hold Contact selected from list fragment
    private Contact selectedContact;

    //arrayList to hold all device contacts
    private ArrayList<Contact> contactsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initializing arrayLists
        contactsList = new ArrayList<>();

        //checking permission
        //if permission is not granted yet, ask for it
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            int PERMISSION_REQUEST_READ_CONTACTS = 1;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    PERMISSION_REQUEST_READ_CONTACTS);
        } else {
            //if permission was previously granted
            //getting the device contacts
            getDeviceContacts();

            //loading the fragments
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.listFragmentContainer, ContactListFragment.newInstance(), ContactListFragment.TAG)
                    .commit();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.detailFragmentContainer, DetailsFragment.newInstance(), DetailsFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //making sure the permission was granted
        //if not, close the activity
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            //getting the device contacts
            getDeviceContacts();

            //loading the fragments
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.listFragmentContainer, ContactListFragment.newInstance(), ContactListFragment.TAG)
                    .commit();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.detailFragmentContainer, DetailsFragment.newInstance(), DetailsFragment.TAG)
                    .commit();

        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //getting the device contacts
        getDeviceContacts();

        //loading the fragments
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.listFragmentContainer, ContactListFragment.newInstance(), ContactListFragment.TAG)
                .commit();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.detailFragmentContainer, DetailsFragment.newInstance(), DetailsFragment.TAG)
                .commit();

    }

    //method to get contacts from device
    private void getDeviceContacts(){

        //clearing the contacts list so that when onResume is called all data is refreshed
        contactsList.clear();

        ContentResolver cr = this.getContentResolver();
        Cursor contactCursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if(Objects.requireNonNull(contactCursor).getCount() > 0){
            while(contactCursor.moveToNext()){

                ArrayList<String> phoneNum = new ArrayList<>();
                Drawable image = getDrawable(R.drawable.cat7);

                String id = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts._ID));

                //getting the name
                String displayName = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                //splitting the name up into first, middle, and last
                String[] splitName = displayName.split(" ");

                //getting the phone numbers
                if (Integer.parseInt(contactCursor.getString(
                        contactCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0){
                    Cursor phoneCursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    System.out.println(phoneCursor);
                    while(Objects.requireNonNull(phoneCursor).moveToNext()){

                        String phone = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phoneNum.add(phone);
                    }
                    phoneCursor.close();
                }

                //getting the contact image
                String imageUri = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                try {
                    if(imageUri != null){
                        Bitmap bitmap = MediaStore.Images.Media .getBitmap(this.getContentResolver(), Uri.parse(imageUri));
                        image = new BitmapDrawable(getResources(), bitmap);
                    } else {
                        image = getDrawable(R.drawable.cat7);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                //creating the new contact
                Contact newContact;

                //selective constructors based on how many names the contact has
                switch (splitName.length){
                    case 1:
                        newContact = new Contact(splitName[0], "", "", phoneNum, id, image);
                        contactsList.add(newContact);
                    case 2:
                        newContact = new Contact(splitName[0], "", splitName[1], phoneNum, id, image);
                        contactsList.add(newContact);
                    case 3:
                        newContact = new Contact(splitName[0], splitName[1], splitName[2], phoneNum, id, image);
                        contactsList.add(newContact);
                    default:
                        break;
                }
            }
        }

        //making sure the device has contacts before trying to display the first contact
        if(contactsList.size() != 0){
            selectedContact = contactsList.get(0);
        }

    }

    //interface method to pass contacts to fragment
    @Override
    public ArrayList<Contact> getContacts() {
        return contactsList;
    }

    //interface method to get the selected contact from the list fragment
    @Override
    public void sendSelected(Contact selected) {
        selectedContact = selected;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.detailFragmentContainer, DetailsFragment.newInstance(), DetailsFragment.TAG)
                .commit();
    }

    //interface method to send the selected contact to the details fragment
    @Override
    public Contact getSelected() {
        return selectedContact;
    }
}
