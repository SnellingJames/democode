//James Snelling
//JAV 2 - 201811
//ContactListFragment.java

package edu.fullsail.snellingjamesce_04.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import edu.fullsail.snellingjamesce_04.Contact;
import edu.fullsail.snellingjamesce_04.R;
import edu.fullsail.snellingjamesce_04.adapters.ContactsListAdapter;

public class ContactListFragment extends ListFragment {

    //variable to hold the contacts
    private ArrayList<Contact> contactsList;

    //tag
    public static final String TAG = "ContactListFragment.TAG";

    //listener
    private ContactListListener mListener;

    //interface
    public interface ContactListListener{
        ArrayList<Contact> getContacts();
        void sendSelected(Contact selected);
    }

    //methods to interact with the interface
    private void sendSelected(Contact selected){
        mListener.sendSelected(selected);
    }

    private ArrayList<Contact> getContactsList(){
       return mListener.getContacts();
    }

    //new instance
    public static ContactListFragment newInstance() {

        return new ContactListFragment();
    }

    //checking implementation
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof ContactListListener){
            mListener = (ContactListListener)context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //getting contacts and setting the list adapter
        contactsList = getContactsList();
        setListAdapter(new ContactsListAdapter(getContext(), contactsList));
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        sendSelected(contactsList.get(position));
    }



}
