//James Snelling
//JAV 2 - 201811
//DetailsFragment.java

package edu.fullsail.snellingjamesce_04.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Objects;

import edu.fullsail.snellingjamesce_04.Contact;
import edu.fullsail.snellingjamesce_04.R;

public class DetailsFragment extends Fragment {


    //tag
    public static final String TAG = "DetailsFragment.TAG";

    //listener variable
    private DetailListener mListener;

    //interface
    public interface DetailListener{
        Contact getSelected();
    }

    //method to interact with interface
    private Contact getSelectedContact(){
        return mListener.getSelected();
    }

    //newInstance
    public static DetailsFragment newInstance() {

        return new DetailsFragment();
    }

    //checking that interface is implemented
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DetailListener){
            mListener = (DetailListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //getting the contact
        Contact selectedContact = getSelectedContact();

        //setting the full name textView and image
        TextView nameText = Objects.requireNonNull(getView()).findViewById(R.id.fullNameTextView);
        ImageView image = getView().findViewById(R.id.contactImageView);
        nameText.setText(selectedContact.getDisplayName());
        image.setImageDrawable(selectedContact.getDisplayImage());

        //displaying the phone numbers
        ListView phoneNumbers = getView().findViewById(R.id.phoneNumbersListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()), android.R.layout.simple_list_item_1, selectedContact.getPhoneNumber());
        phoneNumbers.setAdapter(adapter);
    }
}
