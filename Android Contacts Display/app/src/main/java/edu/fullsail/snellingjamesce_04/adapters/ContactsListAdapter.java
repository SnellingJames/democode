//James Snelling
//JAV 2 - 201811
//ContactsListAdapter.java

package edu.fullsail.snellingjamesce_04.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import edu.fullsail.snellingjamesce_04.Contact;
import edu.fullsail.snellingjamesce_04.R;

public class ContactsListAdapter extends BaseAdapter {


    //variables to make the adapter
    private final Context mContext;
    private final ArrayList<Contact> mArrayList;
    private static final int ID_CONSTANT = 0x01000000;

    //constructor
    public ContactsListAdapter(Context c, ArrayList<Contact> objects) {

        mContext = c;
        mArrayList = objects;
    }

    @Override
    public int getCount() {
        if (mArrayList != null){
            return mArrayList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (mArrayList != null && position<mArrayList.size() && position>=0){
            return mArrayList.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return ID_CONSTANT + position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //variable for ViewHolder
        ContactViewHolder viewHolder;

        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.contacts_list_item, parent, false);

            //creating a new ViewHolder to hold the convertView fields
            viewHolder = new ContactViewHolder();
            viewHolder.nameTextView = convertView.findViewById(R.id.contactNameTextView);
            viewHolder.idTextView = convertView.findViewById(R.id.contactIdTextView);
            viewHolder.primaryPhoneNumberTextView = convertView.findViewById(R.id.contactPhoneTextView);
            viewHolder.contactImage = convertView.findViewById(R.id.contactThumbnail);

            //setting the convertView's tag as the newly created ViewHolder
            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ContactViewHolder) convertView.getTag();

        }

        Contact item = (Contact) getItem(position);

        if (item != null){

            viewHolder.nameTextView.setText(item.getListName());
            viewHolder.primaryPhoneNumberTextView.setText(item.getPhoneNumber().get(0));
            viewHolder.idTextView.setText(item.getId());
            viewHolder.contactImage.setImageDrawable(item.getDisplayImage());
        }

        return convertView;
    }

    static class ContactViewHolder{
        ImageView contactImage;
        TextView nameTextView;
        TextView primaryPhoneNumberTextView;
        TextView idTextView;
    }
}
